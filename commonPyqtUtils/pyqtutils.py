'''
Created on 20/set/2015

@author: Daniel
'''
import os
import sys
import traceback
from commonUtils import commonUtils

try:
    from PyQt4 import QtGui
except Exception as ex:
    commonUtils.logMessage('error', 'PyQt4 not installed. Try with PySide', 'Import QT')
    from PySide import QtGui

# C:\Users\Daniel\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup


def launchTryIconMessage(trayQtObject, title, message, level='info'):
    if level.upper() == 'INFO':
        iconMode = 1    # Info
    elif level.upper() == 'WARNING':
        iconMode = 2    # No warning
    elif level.upper() == 'ERROR':
        iconMode = 3    # No critical
    else:
        iconMode = 0    # No icon
    trayQtObject.showMessage(title, message, iconMode)


def getDirectoryFromSystem(parent=None, pathToOpen=''):
    return str(QtGui.QFileDialog.getExistingDirectory(parent, "Select Directory", pathToOpen))


def getFileFromSystem(desc='Open', startPath='/home/'):
    fileName = QtGui.QFileDialog.getOpenFileName(None, desc, startPath)
    if os.path.exists(fileName):
        return str(fileName)
    return ''


def launchMessage(message='', msgType='message'):
    commonUtils.logMessage('info', message, 'launchMessage')
    messBox = QtGui.QMessageBox()
    messBox.setText(str(message))
    if msgType == 'message':
        messBox.setStandardButtons(QtGui.QMessageBox.Ok)
    elif msgType == 'question':
        messBox.setStandardButtons(QtGui.QMessageBox.Ok | QtGui.QMessageBox.Cancel)
    if (messBox.exec_() == QtGui.QMessageBox.Ok):
        messBox.accept()
        return True
    else:
        return False


def logAndMessageException(ex, message=''):
    traceback.print_exc(file=sys.stdout)
    traceBackMess = traceback.format_exc()
    commonUtils.logMessage('error', ex, 'exceptionManagement')
    commonUtils.logMessage('error', traceBackMess, 'exceptionManagement')
    launchMessage(message + ': %s \n %s' % (ex, traceBackMess))


def getRowsFromListWidget(listWidget):
    outList = []
    outDict = {}
    linesCount = listWidget.count()
    for index in range(0, linesCount):
        listItem = listWidget.item(index)
        cellValue = ''
        if listItem:
            cellValue = str(listItem.text())
        outList.append(cellValue)
        outDict[index] = cellValue
    return outList, outDict


def getRowsFromTableWidget(tableWidget, outType='list', headers=[]):
    rowCount = tableWidget.rowCount()
    columnCount = tableWidget.columnCount()
    if outType == 'list':
        outList = []
        for rowIndex in range(0, rowCount):
            rowList = []
            for colIndex in range(0, columnCount):
                tableItem = tableWidget.item(rowIndex, colIndex)
                cellValue = ''
                if tableItem:
                    cellValue = str(tableItem.text())
                rowList.append(cellValue)
            outList.append(rowList)
        return outList
    elif outType == 'dict' and headers:
        outDict = {}
        for rowIndex in range(0, rowCount):
            for colIndex in range(0, columnCount):
                colName = headers[colIndex]
                tableItem = tableWidget.item(rowIndex, colIndex)
                cellValue = ''
                if tableItem:
                    cellValue = str(tableItem.text())
                    if rowIndex not in outDict:
                        outDict[rowIndex] = {colName: cellValue}
                    else:
                        outDict[rowIndex][colName] = cellValue
        return outDict


def getSelectedRowsFromListWidget(listWidget):
    itemsSelected = listWidget.selectedItems()
    return [str(item.text()) for item in itemsSelected]


def getSelectedRowsFromTableWidget(tableWidget, headers=False, onlyOne=False):
    if not headers:
        headers = []
        for colIndex in range(0, tableWidget.columnCount()):
            headers.append(str(tableWidget.horizontalHeaderItem(colIndex).text()))
    outDict = {}
    selectedItems = tableWidget.selectedItems()
    if onlyOne and len(selectedItems) != 2:
        launchMessage('Too much rows selected!', 'warning')
        return {}
    for itemIndex in selectedItems:
        colIndex = itemIndex.column()
        rowIndex = itemIndex.row()
        colName = headers[colIndex]
        itemText = str(itemIndex.text())
        if rowIndex not in outDict:
            outDict[rowIndex] = {colName: itemText}
        else:
            outDict[rowIndex][colName] = itemText
    return outDict


def commonPopulateTable(headers, values, tableWidget, flags={}):
    '''
        @flags: {'colIndex': flags}
    '''
    outDict = {}
    colCount = len(headers)
    colIndexList = range(0, colCount)
    tableWidget.setColumnCount(colCount)
    tableWidget.setHorizontalHeaderLabels(headers)
    rowPosition = 0
    for menuObj in values:
        try:
            tableWidget.setRowCount(rowPosition + 1)
            rowDict = {}
            for colIndex in colIndexList:
                colName = headers[colIndex]
                colVal = menuObj.__dict__.get(colName, '')
                rowDict[colName] = colVal
                twItem = QtGui.QTableWidgetItem(str(colVal))
                if colIndex in flags:
                    twItem.setFlags(flags[colIndex])
                tableWidget.setItem(rowPosition, colIndex, twItem)
            outDict[rowPosition] = rowDict
            rowPosition = rowPosition + 1
        except Exception as ex:
            launchMessage(ex, msgType='error')
    tableWidget.resizeColumnsToContents()
    tableWidget.horizontalHeader().setStretchLastSection(True)
    return outDict

def getButtonsLayout(buttonsList, orientation='orizontal'):
    if orientation.lower() == 'orizontal':
        mainLay = QtGui.QHBoxLayout()
    else:
        mainLay = QtGui.QVBoxLayout()
    outButtons = []
    for buttonString in buttonsList:
        butt = QtGui.QPushButton(buttonString)
        outButtons.append(butt)
        mainLay.addWidget(butt)
    return mainLay, outButtons

        
    
